const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
const validator = require("validator");

//Define your schema
const memberSchema = new Schema(
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "J"
		},

		lastName: {
			type: String,
			trim: true,
			maxlength: 30,
			default: "Doe"
		},
		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 10,
			index: true,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)) {
					throw new Error("No special characters!")
				}
				console.log("Username has been added")
			}
		},
		position: {
			type: String,
			enum: ["instructor", "student", "hr", "admin", "ca" ],
			required: true
		},
		age: {
			type: Number,
			min: 18,
			required: true
		}
	},
	{
		timestamps: true
	}

);

memberSchema.plugin(uniqueValidator);

//Export your model
module.exports = mongoose.model("Member", memberSchema);
